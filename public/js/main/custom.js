// input get only float number and accept paste && ctrl+v only for numbers
$(document).on("input", ".float", function() {
    // First regex replaces anything that's not a number or a decimal.
    // Second regex removes any instance of a second decimal
    this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
});

// input get only numbers and accept paste && ctrl+v only for numbers
$(document).on("input", ".numeric", function() {
    //allow paste and ctrl+v only for  numbers
    this.value = this.value.replace(/\D/g,''); //if it's not a digit, replace with ''
});

// input get only characters and accept paste && ctrl+v only for numbers
$(document).on("input", ".persian_to_english_char", function() {
    // 1- convert persian char to english char
    var persianChar = [ "ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "چ", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "گ", "ظ", "ط", "ز", "ر", "ذ", "د", "پ","ئ", "و","؟","۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"],
        englishChar = [ "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m","m", ",","?","0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

    var fixCharecters = function (str) {
        if(typeof str === 'string')
        {
            for (var i = 0, charsLen = persianChar.length; i < charsLen; i++) {
                str = str.replace(new RegExp(persianChar[i], "g"), englishChar[i]);
            }
        }
        return str;
    };

    this.value=fixCharecters(this.value);

    // 2- allow paste and ctrl+v only for english characters
    this.value = this.value.replace(/\W+^\@+^\s/g,'');
});

// input get only characters and accept paste && ctrl+v only for numbers
$(document).on("input", ".english_to_persian_char", function() {
    // 1- convert persian char to english char
    var persianChar = [ "ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "چ", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "گ", "ظ", "ط", "ز", "ر", "ذ", "د", "پ", "و","؟","۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹" , "ض", "ص", "ث", "ق", "ف", "غ", "ع", "ه", "خ", "ح", "ج", "چ", "ش", "س", "ی", "ب", "ل", "ا", "ت", "ن", "م", "ک", "گ", "ظ", "ط", "ز", "ر", "ذ", "د", "پ"],
        englishChar = [ "q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "[", "]", "a", "s", "d", "f", "g", "h", "j", "k", "l", ";", "'", "z", "x", "c", "v", "b", "n", "m", ",","?","0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "[", "]", "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "Z", "X", "C", "V", "B", "N", "M" ];

    var fixCharecters = function (str) {
        if(typeof str === 'string')
        {
            for (var i = 0, charsLen = persianChar.length; i < charsLen; i++) {
                str = str.replace(englishChar[i],persianChar[i]);
            }
        }
        return str;
    };

    this.value=fixCharecters(this.value);

    // 2- allow paste and ctrl+v only for english characters
    this.value = this.value.replace(/\W+^\@+^\s/g,'');
});

$(document).on("input", ".justpersian",function(){

    if(!/^[پچجحخهعغفقثصضشسیبلاتنمکگوئدذرزطظژؤإأءًٌٍَُِّ\s\n\r\t\d\(\)\[\]\{\}.,،;\-؛]+$/.test($(this).val())){
        $(this).val("");
    }
});

$(document).on("input", ".justenglish",function(){
    // alert('only english');
    if(!/^[\s\n\r\t\d.'.,@#$%&*()-_*@a-zA-z0-9]+$/.test($(this).val())){
        $(this).val("");
    }
});
