<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
use  App\Http\Controllers\UpdateController;
use  App\Http\Controllers\ColumnsController;
Route::get('/', function () {
    return view('welcome');
});
Route::get('/dashboard', function () {
    return view('test.index');
});
//Route::get('/dashboard/tableForm', function () {
//    return view('table.index');
//});
//Route::post('dashboard/tableForm','App\Http\Controllers\ColumnsController@store');
Route::get('dashboard/getUpdate','App\Http\Controllers\UpdateController@index');
Route::get('dashboard/tableForm','App\Http\Controllers\ColumnsController@index');
Route::post('dashboard/setTable','App\Http\Controllers\ColumnsController@store');
Route::get('/dashboard/patient', function () {
    return view('PacienteDossier.index');
});
//Route::get('/dashboard/patientForm', function () {
//    return view('PacienteDossier.create');
//});
