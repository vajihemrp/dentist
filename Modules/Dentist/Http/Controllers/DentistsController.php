<?php

namespace Modules\Dentist\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Modules\Dentist\Http\Requests\DentistCreateRequest;
use Modules\Dentist\Http\Requests\DentistUpdateRequest;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Modules\Dentist\Repositories\DentistRepository;
use Illuminate\Routing\Controller;

/**
 * Class DentistsController.
 *
 * @package namespace Modules\Dentist\Http\Controllers;
 */
class DentistsController extends Controller
{
    /**
     * @var DentistRepository
     */
    protected $repository;

    /**
     * DentistsController constructor.
     *
     * @param DentistRepository $repository
     */
    public function __construct(DentistRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dentists = $this->repository->all();

        return response()->json([
            'data' => $dentists,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DentistCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DentistCreateRequest $request)
    {
        try {

            $dentist = $this->repository->create($request->all());

            $response = [
                'message' => 'Dentist created.',
                'data'    => $dentist->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dentist = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $dentist,
            ]);
        }

        return view('dentists.show', compact('dentist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dentist = $this->repository->find($id);

        return view('dentists.edit', compact('dentist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DentistUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DentistUpdateRequest $request, $id)
    {
        try {

            $dentist = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Dentist updated.',
                'data'    => $dentist->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Dentist deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Dentist deleted.');
    }
}
