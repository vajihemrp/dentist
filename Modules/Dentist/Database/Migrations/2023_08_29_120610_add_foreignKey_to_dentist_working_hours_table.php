<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dentist_working_hours', function (Blueprint $table) {
            $table->bigInteger('dentist_working_hours_id')->unsigned()->index()->nullable()->afetr('to');
            $table->foreign('dentist_working_hours_id')->references('id')
                ->on('dentists')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dentist_working_hours', function (Blueprint $table) {
            $table->dropForeign(['dentist_working_hours_id']);
            $table->dropColumn(['dentist_working_hours_id']);
        });
    }
};
