<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dentists', function (Blueprint $table) {
            $table->id();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('medical_code');
            $table->string('percent');
            $table->date('birthDate')->nullable();
            $table->boolean('gender')->nullable()->default(0);
            $table->string('bank_name')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('bank_cart_number')->nullable();
            $table->string('specialist')->nullable();
            $table->string('skill')->nullable();
            $table->string('university')->nullable();
            $table->string('reagent')->nullable();
            $table->string('experience')->nullable();
            $table->string('insurance_code')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->boolean('sms_operation')->nullable()->default(0);
            $table->boolean('sms_visit')->nullable()->default(0);
            $table->text('homeAddress')->nullable();
            $table->text('workAddress')->nullable();
            $table->string('showing_sort','2');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dentists');
    }
};
