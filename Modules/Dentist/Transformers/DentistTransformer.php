<?php

namespace Modules\Dentist\Transformers;

use League\Fractal\TransformerAbstract;
use Modules\Dentist\Entities\Dentist;

/**
 * Class DentistTransformer.
 *
 * @package namespace Modules\Dentist\Transformers;
 */
class DentistTransformer extends TransformerAbstract
{
    /**
     * Transform the Dentist entity.
     *
     * @param \Modules\Dentist\Entities\Dentist $model
     *
     * @return array
     */
    public function transform(Dentist $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
