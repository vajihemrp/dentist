<?php

namespace Modules\Dentist\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DentistRepository.
 *
 * @package namespace Modules\Dentist\Repositories;
 */
interface DentistRepository extends RepositoryInterface
{
    //
}
