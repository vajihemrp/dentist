<?php

namespace Modules\Dentist\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Dentist\Repositories\DentistRepository;
use Modules\Dentist\Entities\Dentist;
use Modules\Dentist\Validators\DentistValidator;

/**
 * Class DentistRepositoryEloquent.
 *
 * @package namespace Modules\Dentist\Repositories;
 */
class DentistRepositoryEloquent extends BaseRepository implements DentistRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Dentist::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
