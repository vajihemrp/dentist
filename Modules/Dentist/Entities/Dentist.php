<?php

namespace Modules\Dentist\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Dentist.
 *
 * @package namespace Modules\Dentist\Entities;
 */
class Dentist extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstName','lastName','medical_code','percent','birthDate','gender','bank_name',
        'bank_account_number','bank_cart_number','specialist','skill','university','reagent','experience','insurance_code',
        'phone','mobile','sms_operation','sms_visit','homeAddress','workAddress','showing_sort'];

}
