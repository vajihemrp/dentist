<?php

namespace Modules\Dentist\Presenters;

use Modules\Dentist\Transformers\DentistTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DentistPresenter.
 *
 * @package namespace Modules\Dentist\Presenters;
 */
class DentistPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DentistTransformer();
    }
}
