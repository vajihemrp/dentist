<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('dashboard')->group(function() {
    Route::get('/patients', 'PatientsController@index');
    Route::post('/patients', 'PatientsController@store');
    Route::get('/patients/{patientId}/edit','PatientsController@edit');
    Route::put('/patients','PatientsController@update');
    Route::get('/patients/{id}','PatientsController@show');
    Route::get('/patientForm', 'PatientsController@create');
    Route::post('/del_item/patient','PatientsController@destroy');
});
