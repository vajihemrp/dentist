<?php

namespace Modules\Patient\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Patient\Repositories\PatientRepository;
use Modules\Patient\Entities\Patient;
use Modules\Patient\Validators\PatientValidator;

/**
 * Class PatientRepositoryEloquent.
 *
 * @package namespace Modules\Patient\Repositories;
 */
class PatientRepositoryEloquent extends BaseRepository implements PatientRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Patient::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
