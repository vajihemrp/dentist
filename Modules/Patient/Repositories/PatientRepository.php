<?php

namespace Modules\Patient\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PatientRepository.
 *
 * @package namespace Modules\Patient\Repositories;
 */
interface PatientRepository extends RepositoryInterface
{
    //
}
