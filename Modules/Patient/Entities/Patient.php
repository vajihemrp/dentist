<?php

namespace Modules\Patient\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Patient.
 *
 * @package namespace Modules\Patient\Entities;
 */
class Patient extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected  $table= "patients";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fileNumber','firstName','lastName','sureName','fatherName','certificateId','melliCode',
        'email','gender','marriedStatus','file_start_date',
        'sms','birthDate','birthLocation','bloodGroup','pregnancy','surgery','description','education','job',
        'homePhone','workPhone','mobile','homeAddress','workAddress','customerPic'];



}
