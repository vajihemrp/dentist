<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('fileNumber');
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->string('sureName')->nullable();
            $table->string('fatherName')->nullable();
            $table->string('certificateId')->nullable();
            $table->string('melliCode','10')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->string('marriedStatus')->nullable();
            $table->boolean('sms')->nullable()->default(0);
            $table->date('file_start_date')->nullable();
            $table->date('birthDate')->nullable();
            $table->string('birthLocation')->nullable();
            $table->string('bloodGroup')->nullable();
            $table->boolean('pregnancy')->default(0);
            $table->text('surgery')->nullable();
            $table->text('description')->nullable();
            $table->string('education')->nullable();
            $table->string('job')->nullable();
            $table->string('homePhone')->nullable();
            $table->string('workPhone')->nullable();
            $table->string('mobile')->nullable();
            $table->text('homeAddress')->nullable();
            $table->text('workAddress')->nullable();
            $table->string('customerPic')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
};
