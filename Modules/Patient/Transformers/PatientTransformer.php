<?php

namespace Modules\Patient\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use Modules\Patient\Entities\Patient;
/**
 * Class PatientTransformer.
 *
 * @package namespace Modules\Patient\Transformers;
 */
class PatientTransformer extends TransformerAbstract
{
    /**
     * Transform the Patient entity.
     *
     * @param \Modules\Patient\Entities\Patient $model
     *
     * @return array
     */
    public function transform(Patient $model)
    {
        //$from = date_create(Carbon::now());

        $fileStartDate='';
        $birthDate='';

        if ($model->file_start_date) {

            $dateArray = explode("-", $model->file_start_date );
            $fileStartDate = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        }

        if ($model->birthDate) {
            $dateArray = explode("-", $model->birthDate );
            $birthDate  = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        }
        return [
            'id'         => (int) $model->id,
            'fileNumber'=>$model->fileNumber,
            'firstName'=>$model->firstName,
            'lastName'=>$model->lastName,
            'sureName'=>$model->sureName ? $model->sureName : '',
            'fatherName'=>$model->fatherName ? $model->fatherName : '',
            'certificateId'=>$model->certificateId ? $model->certificateId  : '',
            'melliCode'=>$model->melliCode ? $model->melliCode : '',
            'email'=>$model->email ? $model->email : '',
            'gender'=>$model->gender == 'male' ? 'مرد' : 'زن',
            'marriedStatus'=>$model->marriedStatus ? 'بله' : 'خیر',
            'file_start_date'=>$fileStartDate,
            'sms'=> $model->sms ? 'بله' : 'خیر',
            'birthDate'=>$birthDate,
            'birthLocation'=>$model->birthLocation ? $model->birthLocation : '',
            'bloodGroup'=>$model->bloodGroup ? $model->bloodGroup : '',
            'pregnancy'=>$model->pregnancy ? 'بله' : 'خیر',
            'surgery'=>$model->surgery ? $model->surgery : '',
            'description'=>$model->description ? $model->description : '',
            'education'=>$model->education ? $model->education : '',
            'job'=>$model->job ? $model->job : '',
            'homePhone'=>$model->homePhone ? $model->homePhone : '',
            'workPhone'=>$model->workPhone ? $model->workPhone : '',
            'mobile'=>$model->mobile ? $model->mobile : '',
            'homeAddress'=>$model->homeAddress ? $model->homeAddress : '',
            'workAddress'=>$model->workAddress ? $model->workAddress : '',

        ];
    }
}
