<?php

namespace Modules\Patient\Presenters;

use Modules\Patient\Transformers\PatientTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PatientPresenter.
 *
 * @package namespace Modules\Patient\Presenters;
 */
class PatientPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PatientTransformer();
    }
}
