<?php

namespace Modules\Patient\Http\Controllers;

use App\Helpers\CustomerFileNumber;
use App\Helpers\File;
use App\Helpers\PersianDate;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Modules\Patient\Entities\Patient;
use Modules\Patient\Presenters\PatientPresenter;

use Modules\Patient\Repositories\PatientRepository;
use Illuminate\Routing\Controller;

/**
 * Class PatientsController.
 *
 * @package namespace Modules\Patient\Http\Controllers;
 */
class PatientsController extends Controller
{
    /**
     * @var PatientRepository
     */
    protected $repository;

    /**
     * PatientsController constructor.
     *
     * @param PatientRepository $repository
     */
    public function __construct(PatientRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->setPresenter(PatientPresenter::class);

        $patients = $this->repository->scopeQuery(function ($q) {
            return $q->orderBy('id', 'desc');
        })->paginate('10');

        list($patient, $paginate_items, $patients) = $this->separatePaginationItems($patients);


        return response()->json([
            'draw' => 1,
            'recordsTotal' => 57,
            'recordsFiltered' => 57,
            'data' => $patient[0],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PatientCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(\Modules\Patient\Http\Requests\PatientCreateRequest $request)
    {
//        try {
        date_default_timezone_set("Asia/Tehran");
        $data = $request->except(['customerPic']);


        if ($request->hasFile('customerPic')) {
            $image_path = File::fileUpload($request->customerPic, 'patients/images/profiles', 'public');
            if ($image_path) {
                $data['customerPic'] = $image_path;
            } else {
                $data['customerPic'] = null;
            }
        } else {
            $data['customerPic'] = null;
        }

        $patient = $this->repository->create([
            'fileNumber' => $request->fileNumber,
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'sureName' => $request->sureName,
            'fatherName' => $request->fatherName,
            'certificateId' => $request->certificateId,
            'melliCode' => $request->melliCode,
            'email' => $request->email,
            'gender' => $request->gender,
            'marriedStatus' => $request->marriedStatus,
            'file_start_date' => PersianDate::convertToGregorianDate($request->file_start_date ?? NULL),
            'sms' => $request->sms,
            'birthDate' => PersianDate::convertToGregorianDate($request->birth_date ?? NULL),
            'birthLocation' => $request->birthLocation,
            'bloodGroup' => $request->bloodGroup,
            'pregnancy' => $request->pregnancy,
            'surgery' => $request->surgery,
            'description' => $request->description,
            'education' => $request->education,
            'job' => $request->job,
            'homePhone' => $request->homePhone,
            'workPhone' => $request->workPhone,
            'mobile' => $request->mobile,
            'homeAddress' => $request->homeAddress,
            'customerPic' => $data['customerPic'],
        ]);

        $response = [
            'message' => 'Patient created.',
            'data' => $patient->toArray(),
        ];

        if ($request->wantsJson()) {

            return response()->json($response);
        }

        alert()->success('با موفقیت ثبت شد')->persistent(true, false);

        return redirect()->back();

//        } catch (\Exception $e) {
//
//            if ($request->wantsJson()) {
//                return response()->json([
//                    'error'   => true,
//                    'message' => $e->getMessage()
//                ]);
//            }
//
//            return redirect()->back()->withErrors($e->getMessage())->withInput();
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $patient = $this->repository->findWhere(['id'=>$id]);

            return response()->json([
                'data' => $patient,
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'data' => [],
            ]);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = $this->repository->find($id);

        $lastFileNumber = $this->getLastFileNumber();

        return view('PacienteDossier.edit', compact('item', 'lastFileNumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PatientUpdateRequest $request
     * @param string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(\Modules\Patient\Http\Requests\PatientUpdateRequest $request)
    {
        try {
            $id = $request->id;
            $patient = $this->repository->update([
                'fileNumber' => $request->fileNumber,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'sureName' => $request->sureName,
                'fatherName' => $request->fatherName,
                'certificateId' => $request->certificateId,
                'melliCode' => $request->melliCode,
                'email' => $request->email,
                'gender' => $request->gender,
                'marriedStatus' => $request->marriedStatus,
                'file_start_date' => PersianDate::convertToGregorianDate($request->file_start_date ?? NULL),
                'sms' => $request->sms,
                'birthDate' => PersianDate::convertToGregorianDate($request->birth_date ?? NULL),
                'birthLocation' => $request->birthLocation,
                'bloodGroup' => $request->bloodGroup,
                'pregnancy' => $request->pregnancy,
                'surgery' => $request->surgery,
                'description' => $request->description,
                'education' => $request->education,
                'job' => $request->job,
                'homePhone' => $request->homePhone,
                'workPhone' => $request->workPhone,
                'mobile' => $request->mobile,
                'homeAddress' => $request->homeAddress,
            ], $id);

            if ($request->hasFile('customerPic')) {
                $image_path = File::fileUpload($request->customerPic, 'patients/images/profiles', 'public');
                if ($image_path) {

                    $this->updateOneColumn($patient->id, 'customerePic', $image_path);
                }
            }

            $response = [
                'message' => 'Patient updated.',
                'data' => $patient->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect('/dashboard');
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $deleted = $this->repository->delete($request->id);

            if (request()->wantsJson()) {

                return response()->json([
                    'message' => 'Patient deleted.',
                    'deleted' => $deleted,
                ]);
            }
//        return redirect()->back()->with('message', 'Patient deleted.');

            alert()->success('با موفقیت حذف شد')->persistent(true, false);

            return redirect()->back();

        } catch (\Exception $e) {

            alert()->error('خطایی رخ داده است لطفا با پشتیبانی تماس بگیرید')->persistent(true, false);

            return redirect()->back();
        }

    }

    /* update one column */

    public function updateOneColumn($id, $columnName, $columnValue)
    {
        $this->repository->update([$columnName => $columnValue], $id);
    }

    /**
     * Separate pagination items of l5-repository package to match with front IOrderSummery interface
     *
     * @param $products
     *
     * @return array
     */
    private function separatePaginationItems($patients)
    {
        $patient = array();
        $paginate_items = array();

        foreach ($patients as $key => $value) {
            if ($key === 'meta') {
                array_push($paginate_items, $value['pagination']);
            } else {
                array_push($patient, $value);
            }
        }
        return array($patient, $paginate_items, $patients);
    }

    /**
     * Show the form for create the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lastFileNumber = $this->getLastFileNumber();

        $newFileNumber = $lastFileNumber + 1;

        return view('PacienteDossier.create', compact('newFileNumber', 'lastFileNumber'));
    }

    public function getLastFileNumber()
    {
        $patient = $this->repository->scopeQuery(function ($q) {
            return $q->orderBy('id', 'desc');
        })->first();

        if (!$patient) {
            $lastFileNumber = 1000;
        } else {
            $lastFileNumber = $patient->fileNumber;
        }

        return $lastFileNumber;
    }

}
