<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;

class ApiResponse
{
    /**
     * Send a json api response.
     *
     * @param object|array $data
     * @param string|null $message
     * @param Model|bool $paginate
     * @return \Illuminate\Http\JsonResponse
     */
    public  function sendResponse($data, $message = null, $paginate = false)
    {
        $response = [
            'success' => true,
            'data'    => $data,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * Send a error api response.
     *
     * @param string $error
     * @param array $errorMessages
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public  function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if (!empty($errorMessages)) {
            $response['errors'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}
