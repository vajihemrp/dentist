<?php


namespace App\Helpers;

use App\User;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Storage;


class QR
{
    static  function setQRForProduct($table){

        $products = $table::withTrashed()->with(['locationDetails'])
            ->where('besat_part_code',null)
            ->orwhere('besat_part_code','')->get();

        foreach($products as $product)
        {
            $locs = $product['locationDetails'];

            $locString = '';

            $i = 0;

            foreach ($locs as $loc) {
                $i++;

                $locString .= $loc->store->code . '-' . $loc->salon->code . '-' . $loc->shelf->code . '-' . $loc->floor->code . '-' . $loc->line->code;

                if (sizeof($locs) != $i) {
                    $locString .= ',';
                }
            }

            self::createQRAndPartCode($product,$locString);
        }

        return $products;
    }

    static function setQRForCustomer()
    {
        $items=User::withTrashed()->where('besat_customer_code',null)->orwhere('besat_customer_code','')->get();

        foreach($items as $item)
        {
//        $item->update(['besat_customer_code' => null]);
            $same_code = User::where('created_at',$item->created_at)->orderby('besat_customer_code','desc')->first();
            if(!$same_code || !$same_code->besat_customer_code) {
                $counter = '01';
            }
            else {
                $counter = substr($same_code->besat_customer_code, -2);
                $counter = intval($counter) + 1;
                $counter = $counter < 10 ? '0' . $counter : $counter;
            }

            $time  = strtotime( $item->created_at);
            $day   = date('d',$time);
            $month = date('m',$time);
            $year  = date('y',$time);
            $hour  = date('H',$time);
            $minute = date('i',$time);
            $second = date('s',$time);

            $customer_code='BECC'.$year.$month.$day.$hour.$minute.$second.$counter;

//            $qr_code=QR::generateQR($customer_code,'customers');

            $item->update([
                'besat_customer_code' => $customer_code,
//                'qr_code'=>$qr_code
            ]);
        }
        return;
    }

    static function generateQR($code,$type)
    {
        $image = QrCode::format('png')
            ->size(100)
            ->generate($code);

        $name = 'qr-' . $code . '.png';

////      $output_file = storage_path('app/public').config('variables.picture.qr').$type.'/'.$name;
//        $output_file = Storage::disk('QR')->path('/').$type.'/'.$name;
//        file_put_contents($output_file, $image);
        ////or
        Storage::disk('sftp')->put(config('variables.picture.qr').$type.'/'.$name,$image);
        return $name;
    }

    static function createQRAndPartCode($product, $productLocation): void
    {
        $part_code = 'BEPC' . sprintf('%06d', (string)$product->id.time());

        $code = 'BEPC' . sprintf('%06d', (string)$product->id.time());

//        $code = 'BEPC' . sprintf('%06d', (string)$product->id.time()).'_'. $productLocation;

        $qr_code = QR::generateQR($code, 'products');

        $product->update([
            'besat_part_code' => $part_code,
            'qr_code' => $qr_code
        ]);
    }

    static function createQRAndCode($model, $type): void
    {
        $part_code = $type . sprintf('%02s', (string)$model->code);

        $qr_code = QR::generateQR( $part_code, $type);

        $model->update([
            'part_code' => $part_code,
            'qr_code' => $qr_code
        ]);
    }

    static function createQRPDF($qrs, $names)
    {
        set_time_limit(0);
        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $qr_pdf = new Mpdf([
            'format' => [101.6, 40],
            'mode' => 'utf-8',
            'margin_left' => '1mm',
            'margin_right' => '1mm',
            'margin_top' => '0',
            'margin_bottom' => '0',
            'margin_header' => 0,
            'margin_footer' => 0,
            'fontDir' => array_merge($fontDirs, [
                public_path().'/fonts/IRANSans',
            ]),
            'fontdata' => $fontData + [
                    'iran_sans' => [ //add those (.ttf) files into /public/fonts/IRANSans
                        'R' => 'IRANSansWeb.ttf',// regular font
                        'B' => 'IRANSansWeb_Bold.ttf',// optional: bold font
                        'useOTL' => 0xFF, // required for complicated langs like Persian, Arabic and Chinese
                        'useKashida' => 75, // required for complicated langs like Persian, Arabic and Chinese
                    ]
                ],
            'default_font' => 'iran_sans'
        ]);
//        $qr_pdf->setFooter('{PAGENO}');

        $qr_pdf->WriteHTML(view('Admin.Print.qr-code', compact('qrs','names'))->render());

        $content = $qr_pdf->Output();

        return $content;
    }
}
