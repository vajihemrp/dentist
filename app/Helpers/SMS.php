<?php
/**
 * Created by PhpStorm.
 * User: hoda
 * Date: 9/13/2019
 * Time: 3:28 PM
 */

namespace App\Helpers;
use Kavenegar\KavenegarApi;
use Kavenegar\Exceptions\ApiException;
use Kavenegar\Exceptions\HttpException;
use App\GeneralSetting;
//use UxWeb\SweetAlert\SweetAlert;

class SMS
{
    public static  function SendSMS($message,$receptor,$return=false,$user_invited=null)
    {
        try
        {
//            if using POST method to send sms
            $settings = GeneralSetting::first();
            if ($settings->sms_verification == 1) {
            $api=$settings->sms_api;
            $sender =$settings->sms_sender;
            $api = new KavenegarApi($api);
            $result = $api->Send($sender,$receptor, htmlentities($message, ENT_QUOTES | ENT_IGNORE, "UTF-8"));

            if($result && $return && $user_invited)
            {
                $user_invited->Increment('repeat');
                //SweetAlert::success($result[0]->statustext)->persistent('بستن');
                //return back();
                return redirect()->back()->with('success', $result[0]->statustext);
            }

            return $result;
            }
        }
        catch(ApiException $e){
            if(!$return)
                return $e->errorMessage();

            return redirect()->back()->withErrors($e->errorMessage());

        }
        catch(HttpException $e){
            if(!$return)
               return $e->errorMessage();
            return redirect()->back()->withErrors($e->errorMessage());

        }
    }
}