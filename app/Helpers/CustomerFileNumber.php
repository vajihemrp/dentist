<?php


namespace App\Helpers;


class CustomerFileNumber
{
    static function createCustomerFileNumber($productId)
    {
        $code = sprintf('%06d', (string)$productId.time());

        return $code;
    }
}
