<?php
/**
 * Created by PhpStorm.
 * User: hoda
 * Date: 11/3/2019
 * Time: 12:38 PM
 */

namespace App\Helpers;


use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;

class PersianDate
{
   static  function convertToJalaliDate($date){
       $dt = new Carbon($date);
       $v=verta($dt);
       $date=$v->year.'/'.$v->month.'/'.$v->day;
       $min=$v->minute <10 ?'0'.$v->minute : $v->minute;
       $time=$v->hour.':'.$min;

       return $date.' '.$time;
   }

    static function convertToGregorian($persian_date)
    {
        if(!$persian_date) {

            return $persian_date;
        }
        else
        {
            $date = str_replace('/', '-', $persian_date);
            $date = explode('-', $date);
            if(count($date)===3)
            {
                $validate=Verta::isValideDate($date[0], $date[1], $date[2]);
                if(!$validate) {
                    return NULL;
                }
                else {

                    $date = Verta::getGregorian($date[0], $date[1], $date[2]);
                    return $date[0] . '-' . $date[1] . '-' . $date[2];
                }

            }

        }

    }

    static function persianDate(Carbon $dt): array
    {
        $v = verta($dt);

        $data = ConvertNumber::to_persian_num($v->year . '/' . $v->month . '/' . $v->day);
        $min = $v->minute < 10 ? '0' . $v->minute : $v->minute;
        $time = ConvertNumber::to_persian_num($v->hour . ':' . $min);
        return array($data, $time);
    }

    static function convertToGregorianDate($persian_date)
    {
        if(!$persian_date) {

            return $persian_date;
        }

        $date_array = explode('/', $persian_date);

        $gd = jalali_to_gregorian($date_array[0], $date_array[1], $date_array[2], '-');

        return $gd;
    }

    static  function convertToJalali($date){
        if(!$date) {

            return $date;
        }

        $dt = new Carbon($date);

        $v=verta($dt);

        $date=$v->year.'/'.$v->month.'/'.$v->day;

        return $date;
    }

    static function convertJalaliToGregorianBYJDF($Date)
    {
        $datetimeArray = explode(" ", $Date);
        $dateArray = explode("-", $datetimeArray['0'] );
        $dateArray = gregorian_to_jalali($dateArray[0], $dateArray[1], $dateArray[2], '/');
        $jalaliDate = $dateArray." ".$datetimeArray[1];

        return $jalaliDate;
    }

    static function getYears($year)
    {
        if ($year) {
            $thisYear = self::convertToGregorianDate($year.'/01/01');

            $lastDate = (string)($year - 1);
            $lastYear =  self::convertToGregorianDate($lastDate.'/01/01');

            $nextDate = (string)($year + 1);
            $nextYear =  self::convertToGregorianDate($nextDate.'/01/01');

        }else {
            $pdate = verta();

            $persianYear = (string)$pdate->year;

            $thisYear = self::convertToGregorianDate($persianYear . '/01/01');

            $lastDate = (string)($pdate->year - 1);
            $lastYear = self::convertToGregorianDate($lastDate . '/01/01');

            $nextDate = (string)($pdate->year + 1);
            $nextYear = self::convertToGregorianDate($nextDate . '/01/01');
        }

        return [$lastYear, $thisYear, $nextYear];
    }
}
