<?php

use App\GeneralSetting;
use App\Sms;
use App\Email;
use Kavenegar\KavenegarApi;

if (!function_exists('send_email')) {

    function send_email($to, $customer_name, $subject,$date,$parcel_tracking_number,$emailID) {

        $settings = GeneralSetting::first();
        if ($emailID >0) {
            $setting = Email::find($emailID);
            $template = $setting->message;
        }else{
            $template = $settings->email_template;
        }

        $from = $settings->email_sent_from;
        if ($settings->email_verification == 1) {

            $headers = "From:$settings->website_title  <$from> \r\n";
            $headers .= "Reply-To: $settings->website_title <$from> \r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $template = str_replace("{{name}}", $customer_name, $template);
            $template = str_replace("{{tracking_number}}", $parcel_tracking_number, $template);
            $template = str_replace("{{date}}", $date, $template);

            mail($to, $subject, $template, $headers);
        }
    }

}

function send_email_reset($to, $name, $subject, $message) {

    $settings = GeneralSetting::first();
    $template = $settings->email_template;
    $from = $settings->email_sent_from;
    if ($settings->email_verification == 1) {

        $headers = "From:$settings->website_title  <$from> \r\n";
        $headers .= "Reply-To: $settings->website_title <$from> \r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $mm = str_replace("{{name}}", $name, $template);
        $message = str_replace("{{message}}", $message, $mm);

        mail($to, $subject, $message, $headers);
    }
}

if (!function_exists('send_sms')) {

//    if using Get method to send sms
//    the sms api like: https://api.infobip.com/api/v3/sendsms/plain?user=****&amp;password=*****&amp;sender=Runner&amp;SMSText={{message}}&amp;GSM={{number}}&amp;type=longSMS
//    function send_sms($message,$to,$id) {
//        $settings = Sms::find($id);
//
//        if ($settings->sms_verification == 1) {
//
//            $sendtext = urlencode($message);
//
//            $appi = $settings->sms_api;
//
//            $appi = str_replace("{{number}}", $to, $appi);
//            $appi = str_replace("{{message}}", $sendtext, $appi);
//            $result = file_get_contents($appi);
//        }
//    }

    function send_sms($date,$receptor_name,$receptor_mobile,$parcel_tracking_number,$return,$smsId)
    {
        try
        {
//            if using POST method to send sms
            $settings = GeneralSetting::first();
            if ($settings->sms_verification == 1) {
                $api=$settings->sms_api;
                $sender =$settings->sms_sender;
                $api = new KavenegarApi($api);

                $sms = Sms::find($smsId);
                $sendtext = $sms->message;
                $sendtext = str_replace("{{name}}", $receptor_name, $sendtext);
                $sendtext = str_replace("{{tracking_number}}", $parcel_tracking_number, $sendtext);
                $sendtext = str_replace("{{date}}", $date, $sendtext);

                $result = $api->Send($sender,$receptor_mobile, htmlentities($sendtext, ENT_QUOTES | ENT_IGNORE, "UTF-8"));

                if($result && $return )
                {
//                $user_invited->Increment('repeat');
                    //SweetAlert::success($result[0]->statustext)->persistent('بستن');
                    //return back();
                    return redirect()->back()->with('success', $result[0]->statustext);
                }

                return $result ;
            }
            return 0;
        }
        catch(ApiException $e){
            if(!$return)
                return $e->errorMessage();

            return redirect()->back()->withErrors($e->errorMessage());

        }
        catch(HttpException $e){
            if(!$return)
                return $e->errorMessage();
            return redirect()->back()->withErrors($e->errorMessage());

        }
    }

}

if (!function_exists('month_arr')) {

    function month_arr() {
        return [
            1=>'January',
            2=>'February',
            3=>'March',
            4=>'April',
            5=>'May',
            6=>'June',
            7=>'July',
            8=>'August',
            9=>'September',
            10=>'October',
            11=>'November',
            12=>'Decembar'
        ];
    }

}
?>