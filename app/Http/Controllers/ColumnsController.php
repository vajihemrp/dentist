<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Column;
class ColumnsController extends Controller
{
	private $column;
    public function __construct(Column $column){
		$this->column= $column;
	}
	public function index(){
		$columns= $this->column->get();
		return view('table.index',compact('columns'));
	}
	public function store(Request $request){
		//return $request;
		$column= $this->column->create(['name'=>$request->columnName]);
		
		$columns= $this->column->get();
		
		return view('table.index',compact('columns'));
	}
}
