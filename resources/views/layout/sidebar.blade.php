<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="/">
                    <i class="fa fa-dashboard"></i>
                    <span>داشبورد</span>
                </a>
            </li>
			<li>
                <a href="{{url('/dashboard/tableForm')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>ثبت ستون جدول</span>
                </a>
            </li>
            <li>
                <a href="{{url('/dashboard/patientForm')}}">
                    <i class="fa fa-file"></i>
                    <span>ثبت پرونده بیمار</span>
                </a>
            </li>
            <li>
                <a href="{{url('/dashboard/patient')}}">
                    <i class="fa fa-files-o"></i>
                    <span>مشاهده پرونده بیمار</span>
                </a>
            </li>

            <li>
                <a href="{{url('/dashboard/dentistForm')}}">
                    <i class="fa fa-user"></i>
                    <span>ثبت دکتر</span>
                </a>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
