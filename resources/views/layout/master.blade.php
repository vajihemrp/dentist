<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>@yield('title','پنل کاربری') - @yield('page')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Bootstrap RTL CSS -->
    <link href="{{asset('css/bs-rtl/bootstrap-rtl.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/bs-rtl/bootstrap-flipped.min.css')}}'" rel="stylesheet">

    <link href="{{asset('css/bootstrap-reset.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />

    <!--right slidebar-->
    <link href="{{asset('css/slidebars.css')}}" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/style-responsive.css')}}" rel="stylesheet" />

    <link rel="stylesheet"
          href="{{asset('assets/MD-PersianDateTime-master-bs3/MD-Bootstrap-persian/Content/MdBootstrapPersianDateTimePicker/PersianDateTimePicker.css')}}"
          xmlns="http://www.w3.org/1999/html"/>

    <!-- Chosen -->
    <link href="{{asset('assets/chosen/chosen.css')}}" rel="stylesheet">

    <!-- DataTable -->
    <link href="{{asset('assets/data-tables-full-2023/datatables.min.css')}}" rel="stylesheet">

{{--    <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">--}}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('js/html5shiv.js')}}"></script>
    <script src="{{asset('js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body>

<section id="container" class="">
    @include('sweetalert::alert')
    @include('layout.header')
    @include('layout.sidebar')
    <!--main content start-->
        <section id="main-content">
            <section class="wrapper site-min-height">
                <!-- page start-->
{{--                Page content goes here--}}
                <section class="wrapper">
                    <div id="app">
                        @yield('content')
                    </div>
                </section>
                <!-- page end-->
            </section>
        </section>
    <!--main content end-->
    @include('layout.footer')

</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{asset('js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('js/slidebars.min.js')}}"></script>
<script src="{{asset('js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<script src="{{asset('js/respond.min.js')}}" ></script>

{{--  chosen select --}}
<script src="{{asset('assets/chosen/chosen.jquery.js')}}"></script>

{{--  mdbootstrap  persian calendar--}}
{{--    https://github.com/Mds92/MD.BootstrapPersianDateTimePicker/tree/master-bs3--}}
<script src="{{asset('assets/MD-PersianDateTime-master-bs3/MD-Bootstrap-persian/Scripts/bootstrap.min.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/MD-PersianDateTime-master-bs3/MD-Bootstrap-persian/Scripts/MdBootstrapPersianDateTimePicker/jalaali.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/MD-PersianDateTime-master-bs3/MD-Bootstrap-persian/Scripts/MdBootstrapPersianDateTimePicker/PersianDateTimePicker.js')}}" type="text/javascript"></script>

{{--  dateTable--}}
<script src="{{asset('assets/data-tables-full-2023/pdfmake-0.2.7/pdfmake.min.js')}}" ></script>
<script src="{{asset('assets/data-tables-full-2023/pdfmake-0.2.7/vfs_fonts.js')}}" ></script>
<script src="{{asset('assets/data-tables-full-2023/datatables.min.js')}}" ></script>

<!--common script for all pages-->
<script src="{{asset('js/common-scripts.js')}}"></script>

<script src="{{asset('js/sweetalert2.all.min.js')}}"></script>
<script>
 $("#updateBtn").click(function () {
		// console.log('yes');
        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({

            type: "GET",

            url: '/dashboard/getUpdate',

            dataType: 'json',

            success: function (response) {

              //console.log({'response':response});
			  //console.log('ok ajax');

			  $('#result').html('');
			  $('#result').html(response);
            }
        });

    });
</script>
@yield('script')
</body>
</html>
