@extends('layout.master')
@section('page')
    بروزرسانی
@endsection
@section('content')
<div class="col-lg-12" dir="rtl">
    <section class="panel">
	<div class="row">
	<div class="col-lg-12">
		<button type="button"  class="btn btn-primary"  id="updateBtn">بروزرسانی</button>
		<div id="result"></div>

	</div>
	
	<div class="col-lg-12" dir="rtl">

       <table class="table table-responsive table-striped table-advance table-hover col-xs-12" id="recordsTable">
            <thead>
            <tr>
                <th class="text-center" id="files-th" title="تصویر قطعه">تصویر قطعه</th>
                <th class="text-center" title="نام قطعه">نام قطعه</th>
                <th class="text-center category_th" title="دسته بندی ها">دسته بندی ها</th>
                <th class="text-center" id="description_th" title="توضیحات">توضیحات</th>

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
	</div>

    </section>
</div>
@endsection