@extends('layout.master')
@section('page')
    بروزرسانی
@endsection
@section('content')
<div class="col-lg-12" dir="rtl">
    <section class="panel">
	<div class="row">
	
	<div class="col-lg-12" dir="rtl">
	<header class="panel-heading">
                              ثبت اطلاعات ستون های جدول
    </header>
                          <div class="panel-body">
                              <form class="form-inline" role="form" action="{{url('dashboard/setTable')}}" method="post" enctype="multipart/form-data">
                                 @csrf
								 <div class="form-group">
                                      <label class="sr-only" for="columnName">نام ستون</label>
                                      <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>
                                  </div>                               
                                  
                                  <button type="submit" class="btn btn-success">ثبت</button>
                              </form>

                          </div>
     
	</div>
	
    </section>
	
	<section class="panel">
	<div class="row">
	
	<div class="col-lg-12" dir="rtl">
	<header class="panel-heading">
                              جدول
    </header>
    <div class="panel-body">
	<div class="col-lg-12" dir="rtl">

       <table class="table table-responsive table-striped table-advance table-hover col-xs-12" id="recordsTable">
            <thead>
            <tr>
			@foreach( $columns as $column)
                <th class="text-center" id="files-th" >{{$column->name}}</th>
                
			@endforeach

            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
	</div>
                              
    </div>
     
	</div>

    </section>
</div>
@endsection