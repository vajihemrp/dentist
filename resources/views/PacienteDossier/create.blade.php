@extends('layout.master')
@section('page')
    ثبت برونده بیمار
@endsection
@section('content')
    <div class="col-lg-12" dir="rtl">
        <section class="panel">
            <div class="row">

                <div class="col-xs-12" dir="rtl">
                    <header class="panel-heading">
                        ثبت اطلاعات ستون های جدول
                    </header>
                    <div class="panel-body">
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" id="patientForm"  role="form" action="{{url('dashboard/patients')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="col-xs-12 col-md-6">
                                    <p> <b>آخرین شماره پرونده :</b> {{ $lastFileNumber }}</p>
                                    <div class="form-group ">
                                        <label for="fileNumber" class="control-label col-lg-2">شماره برونده</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="fileNumber" name="fileNumber" type="text" value="{{$newFileNumber}}"  required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">نام</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="firstname" name="firstName" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="lastname" class="control-label col-lg-2">نام خانوادگی</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="lastname" name="lastName" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surename" class="control-label col-lg-2">نام مستعار</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="surename" name="sureName" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="fatherName" class="control-label col-lg-2">نام بدر</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="fatherName" name="fatherName" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="certificateId" class="control-label col-lg-2">شماره شناسنامه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="certificateId" name="certificateId" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="melliCode" class="control-label col-lg-2">کدملی</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="melliCode" name="melliCode" type="text" />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="email" class="control-label col-lg-2">ایمیل</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="email" name="email" type="email" />
                                        </div>
                                    </div>
{{--                                    <div class="form-group ">--}}
{{--                                        <label for="agree" class="control-label col-lg-2 col-sm-3">Agree to Our Policy</label>--}}
{{--                                        <div class="col-lg-10 col-sm-9">--}}
{{--                                            <input  type="checkbox" style="width: 20px" class="checkbox form-control" id="agree" name="agree" />--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group ">--}}
{{--                                        <label for="newsletter" class="control-label col-lg-2 col-sm-3">Receive the Newsletter</label>--}}
{{--                                        <div class="col-lg-10 col-sm-9">--}}
{{--                                            <input  type="checkbox" style="width: 20px" class="checkbox form-control" id="newsletter" name="newsletter" />--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">جنسیت</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="gender" id="optionsRadios1" value="male" checked>
                                                    مرد
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="gender" id="optionsRadios2" value="female">
                                                    زن
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">تاهل</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="marriedStatus" id="optionsRadios3" value="single" checked>
                                                    مجرد
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="marriedStatus" id="optionsRadios4" value="married">
                                                    متاهل
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">ارسال بیامک</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms" id="optionsRadios5" value="1" checked>
                                                    بله
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms" id="optionsRadios6" value="0">
                                                    خیر
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">گروه خونی</label>
                                        <div class="col-lg-10">
                                            <select class="form-control m-bot15" name="bloodGroup">
                                                <option value="">نامشخص</option>
                                                <option value="A+">A+</option>
                                                <option value="A-">A-</option>
                                                <option value="B+">B+</option>
                                                <option value="B-">B-</option>
                                                <option value="O+">O+</option>
                                                <option value="O-">O-</option>
                                                <option value="AB+">AB+</option>
                                                <option value="AB-">AB-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">وضعیت بارداری</label>
                                        <div class="col-lg-10">
                                            <select class="form-control m-bot15" name="pregnancy">
                                                <option value="1">بله</option>
                                                <option value="0">خیر</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="fromDate1" class="control-label col-lg-2">تاریخ تشکیل برونده</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="fileStartDate"
                                                   data-mddatetimepicker="true" data-trigger="click" onkeypress="return false;"
                                                   data-targetselector="#fileStartDate" data-groupid="group2" data-fromdate="true"
                                                   data-enabletimepicker="false" data-placement="top" name="file_start_date"
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surgery" class="control-label col-lg-2">سابقه جراحی</label>
                                        <div class="col-lg-10">
                                            <textarea name="surgery" class="form-control description_box" rows="3"
                                                      id="surgery"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surgery" class="control-label col-lg-2">توضیحات</label>
                                        <div class="col-lg-10">
                                            <textarea name="description" class="form-control description_box" rows="3"
                                                      id="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">شغل</label>
                                        <div class="col-lg-10">
                                            <select class="form-control m-bot15" name="job">
                                                <option value="">نامشخص</option>
                                                <option value="free">آزاد</option>
                                                <option value="teacher">آموزگار</option>
                                                <option value="hairStylist">آرایشگاه</option>
                                                <option value="employee">کارمند</option>
                                                <option value="nurse">برستار</option>
                                                <option value="accouter">حسابدار</option>
                                                <option value="student">دانشجو</option>
                                                <option value="doctor">بزشک</option>
                                                <option value="midwife" >ماما</option>
                                                <option value="technician">تکنسین</option>
                                                <option value="repairman">تعمیرکار</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">تحصیلات</label>
                                        <div class="col-lg-10">
                                            <select class="form-control m-bot15" name="education">
                                                <option value="">نامشخص</option>
                                                <option value="dyploma">دیبلم</option>
                                                <option value="masterDyploma">فوق دیبلم</option>
                                                <option value="master">لیسانس</option>
                                                <option value="msc">فوق لیسانس</option>
                                                <option value="doctor">دکتری</option>
                                                <option value="student">دانش آموز</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="fromDate1" class="control-label col-lg-2">تاریخ تولد</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="birthDate"
                                                   data-mddatetimepicker="true" data-trigger="click" onkeypress="return false;"
                                                   data-targetselector="#birthDate" data-groupid="group2" data-fromdate="true"
                                                   data-enabletimepicker="false" data-placement="top" name="birth_date"
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="birthLocation" class="control-label col-lg-2">محل تولد</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="birthLocation" name="birthLocation" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="homePhone" class="control-label col-lg-2">تلفن منزل</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="homePhone" name="homePhone" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="workPhone" class="control-label col-lg-2">تلفن محل کار</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="workPhone" name="workPhone" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="mobile" class="control-label col-lg-2">تلفن همراه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="mobile" name="mobile" type="text" />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="homeAddress" class="control-label col-lg-2">آدرس منزل</label>
                                        <div class="col-lg-10">
                                            <textarea name="homeAddress" class="form-control description_box" rows="3"
                                                      id="homeAddress"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="workAddress" class="control-label col-lg-2">آدرس محل کار</label>
                                        <div class="col-lg-10">
                                            <textarea name="workAddress" class="form-control description_box" rows="3"
                                                      id="workAddress"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label" for="Picture">انتخاب عکس</label>
                                        {{-- <span class="red">*</span>--}}
                                        <output id="FilesInfo" >
                                        </output>

                                        <input type="file" id="File" name="customerPic"
                                               class="form-control" multiple
                                               title="انتخاب تصویر قطعه"
                                        />

                                        <div class="help-block with-errors"></div>
                                    </div>

{{--                                    <div class="row">--}}
{{--                                        <div class="form-group " title="جهت تمامی تصاویر، درحالت انتخاب قرار گیرد">--}}
{{--                                            <label for="removePic-checkbox" class="control-label col-lg-9 col-sm-9 checkbox-label"--}}
{{--                                                   style="margin-top: 12px;padding-right: 0">حذف تمام تصاویر موجود</label>--}}
{{--                                            <div class="col-lg-3 col-sm-3">--}}
{{--                                                <input type="checkbox" style="width: 20px" class="checkbox form-control"--}}
{{--                                                       id="removePic-checkbox" name="removePic"/>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="patientFile" class="control-label col-lg-2">عکس</label>--}}
{{--                                        <div class="col-lg-10">--}}
{{--                                            <input type="file" id="patientFile" name="customerPic">--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label class="control-label col-md-3">Without input</label>--}}
{{--                                        <div class="controls col-md-9">--}}
{{--                                            <div class="fileupload fileupload-new" data-provides="fileupload">--}}
{{--                                                <span class="btn btn-white btn-file">--}}
{{--                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i>انتخاب فایل</span>--}}
{{--                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> تغییر</span>--}}
{{--                                                <input type="file" class="default" />--}}
{{--                                                </span>--}}
{{--                                                <span class="fileupload-preview" style="margin-left:5px;"></span>--}}
{{--                                                <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                </div>
                                <br/>
                                <div class="col-xs-12 col-md-12" style="margin-top: 20px">
                                    <div class=" form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-danger" type="submit">ثبت</button>
                                            <button class="btn btn-default" type="button">لغو</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <p id="test"></p>
                                </div>

                            </form>
                        </div>
{{--                        <form class="form-inline" role="form" action="{{url('dashboard/setTable')}}" method="post" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="col-xs-12 col-md-6">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="fileNumber">شماره برونده</label>--}}
{{--                                    <input type="text" name="fileNumber" class="form-control" id="fileNumber" placeholder="شماره برونده" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="sr-only" for="columnName">نام ستون</label>--}}
{{--                                    <input type="text" name="columnName" class="form-control" id="columnName" placeholder="نام ستون" required>--}}
{{--                                </div>--}}

{{--                            </div>--}}


{{--                            <button type="submit" class="btn btn-success">ثبت</button>--}}
{{--                        </form>--}}

                    </div>

                </div>
            </div>
        </section>
    </div>
    <div class="col-lg-12" dir="rtl">
        <section class="panel">
            <div class="row">
                <div class="col-xs-12">
                    <header class="panel-heading">نمایش اطلاعات</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="patientTable" class="table table-hover table-bordered dataTables-example align-items-center"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" >بیماری سیستماتیک بیماری</th>
                                    <th class="text-center">شکایت اصلی</th>
                                    <th class="text-center">توضیحات</th>
                                    <th class="text-center">تاریخ شروع بیماری</th>
{{--                                    <th style="width: 2%;">Weight(kg)</th>--}}
{{--                                    <th style="text-align: center">Order Placement Date</th>--}}
{{--                                    <th class="text-center">status</th>--}}
{{--                                    <th class="text-center">Operation</th>--}}
                                </tr>
                                </thead>
                                <tbody id="company_result">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
@section('script')
    <script>
        {{--let url_path = '{{url("/account")}}';--}}
        let url_path = "{{url('/dashboard/patients')}}";
    </script>
    <script>
        new DataTable('#patientTable', {
          /*  ajax: {
                url: url_path,
                type: 'GET'
            },*/
            columns: [
                { data: 'first_name' },
                { data: 'last_name' },
                { data: 'position' },
                { data: 'office' },
            ],
        /*    processing: true,
            serverSide: true*/
        });
    </script>
    {{-- upload and show files--}}
    <script>
        function fileSelect(input, id, evt) {

            if (window.File && window.FileReader && window.FileList && window.Blob) {

                var files = evt.target.files;

                var result = '';

                var file;

                document.getElementById(id).innerHTML = "";

                var div = document.createElement('div');

                $.each(files, function (key, file) {

                    // if the file is not an image, continue
                    if (!file.type.match('image.*')) {
                        $('#' + input).val('');
                        $('#' + id).html('');
                        alert('لطفا یک فایل تصویری انتخاب کنید.');
                        return false;
                    }

                    if (file.size > 55000) {
                        $('#' + input).val('');
                        $('#' + id).html('');
                        alert('لطفا  تصویری با حجم کمتر از 50KB انتخاب کنید.');
                        return false;
                    }

                    reader = new FileReader();

                    reader.onload = (function (tFile) {

                        return function (evt) {
                            // document.getElementById(id).innerHTML = "";

                            // var div = document.createElement('div');

                            div.innerHTML += '<img style="51px;height:51px;" src="' + evt.target.result + '" />';

                            // document.getElementById(id).appendChild(div);

                            var image = new Image();

                            //Set the Base64 string return from FileReader as source.
                            image.src = evt.target.result;

                            //Validate the File Height and Width.
                            image.onload = function () {
                                var height = this.height;
                                var width = this.width;

                                if (height > 450 || height < 350  || width > 450 || width < 350 ) {
                                    // empty input
                                    $('#' + input).val('');
                                    $('#' + id).html('');

                                    //show width and height to user
                                    alert("تصویری با ابعاد 400*400 پیکسل انتخاب کنید ");
                                    return false;
                                }
                                return true;
                            };
                        };

                    }(file));

                    reader.readAsDataURL(file);
                });

                document.getElementById(id).appendChild(div);

            } else {
                alert('The File APIs are not fully supported in this browser.');
            }
        }

        document.getElementById('File').addEventListener('change', function (e) {

            fileSelect('File', 'FilesInfo', e);
        }, false);

    </script>
@endsection
