@extends('layout.master')
@section('page')
    ثبت برونده بیمار
@endsection
@section('content')
    <div class="col-lg-12" dir="rtl">
        <section class="panel">
            <div class="row">

                <div class="col-xs-12" dir="rtl">
                    <header class="panel-heading">
                       جستجوی بیمار
                    </header>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-lg-10">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="marriedStatus" id="optionsRadios3" value="single" checked >
                                        مشاهده بیمار فعال بر اساس جستجو
                                    </label>
                                    <div class="row">
                                        <div>
                                            <div class="form-group" id="patientSelectbox">

{{--                                                <select data-placeholder="نام بیمار یا شماره پرونده مورد نظر را انتخاب کنید"--}}
{{--                                                        class="form-control chosen-select chosen-rtl "--}}
{{--                                                        tabindex="2" id="patients"--}}
{{--                                                        name="patients[]" multiple--}}
{{--                                                        title="انتخاب ویژگی قطعه">--}}
{{--                                                    <option value="">انتخاب کنید</option>--}}
{{--                                                    @foreach($properties as $property)--}}
{{--                                                        <option value="{{$property->id}}">{{$property->property}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                </select>--}}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="marriedStatus" id="optionsRadios4" value="married" onChange="showAllPatientTable()">
                                        مشاهده همه بیماران
                                    </label>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </section>
    </div>
    <div class="col-lg-12" dir="rtl" style="display: none" id="showOne">
        <section class="panel">
            <div class="row">
                <div class="col-xs-12">
                    <header class="panel-heading">نمایش اطلاعات</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="patientTable" class="table table-hover table-bordered dataTables-example align-items-center"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" >شماره پرونده</th>
                                    <th class="text-center" >نام</th>
                                    <th class="text-center">نام خانوادگی</th>
                                    <th class="text-center">نام مستعار</th>
                                    <th class="text-center">نام پدر</th>
                                    {{--                                    <th style="width: 2%;">Weight(kg)</th>--}}
                                    {{--                                    <th style="text-align: center">Order Placement Date</th>--}}
                                    <th class="text-center">شماره شناسنامه</th>
                                    <th class="text-center">کدملی</th>
                                    <th class="text-center">ایمیل</th>
                                    <th class="text-center">جنسیت</th>
                                    <th class="text-center">تاهل</th>
                                    <th class="text-center">ارسال پیامک</th>
                                    <th class="text-center">تاریخ تشکیل پرونده</th>
                                    <th class="text-center">گروه خونی</th>
                                    <th class="text-center">وضعیت بارداری</th>
                                    <th class="text-center">سابقه جراحی</th>
                                    <th class="text-center">توضیحات</th>
                                    <th class="text-center">تحصیلات</th>
                                    <th class="text-center">شغل</th>
                                    <th class="text-center">تاریخ تولد</th>
                                    <th class="text-center">محل تولد</th>
                                    <th class="text-center">تلفن منزل</th>
                                    <th class="text-center">تلفن محل کار</th>
                                    <th class="text-center">تلفن همراه</th>
                                    <th class="text-center">آدرس منزل</th>
                                    <th class="text-center">آدرس محل کار</th>

                                </tr>
                                </thead>
                                <tbody id="patient_result">

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
{{--                            <button type="button" class="btn btn-primary" id="addButton_one">افزودن </button>--}}
                            <button type="button" class="btn btn-info" id="editButton_one">ویرایش</button>
                            <button type="button" class="btn btn-danger" id="destroyButton_one">حذف (غیرفعال سازی) </button>
                            <form method="post"  id="delete-patient-form-oneShow" action="{{url('dashboard/del_item/patient')}}" style="display: none">
                                @csrf
                                <input type="hidden" id="patient_item_oneShow" name="id" value="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-12" dir="rtl" style="display: none" id="showAll">
        <section class="panel">
            <div class="row">
                <div class="col-xs-12">
                    <header class="panel-heading">نمایش اطلاعات</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="allPatientTable" class="table table-hover table-bordered dataTables-example align-items-center"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" >شماره پرونده</th>
                                    <th class="text-center" >نام</th>
                                    <th class="text-center">نام خانوادگی</th>
                                    <th class="text-center">نام مستعار</th>
                                    <th class="text-center">نام پدر</th>
                                    {{--                                    <th style="width: 2%;">Weight(kg)</th>--}}
                                    {{--                                    <th style="text-align: center">Order Placement Date</th>--}}
                                    <th class="text-center">شماره شناسنامه</th>
                                    <th class="text-center">کدملی</th>
                                    <th class="text-center">ایمیل</th>
                                    <th class="text-center">جنسیت</th>
                                    <th class="text-center">تاهل</th>
                                    <th class="text-center">ارسال پیامک</th>
                                    <th class="text-center">تاریخ تشکیل پرونده</th>
                                    <th class="text-center">گروه خونی</th>
                                    <th class="text-center">وضعیت بارداری</th>
                                    <th class="text-center">سابقه جراحی</th>
                                    <th class="text-center">توضیحات</th>
                                    <th class="text-center">تحصیلات</th>
                                    <th class="text-center">شغل</th>
                                    <th class="text-center">تاریخ تولد</th>
                                    <th class="text-center">محل تولد</th>
                                    <th class="text-center">تلفن منزل</th>
                                    <th class="text-center">تلفن محل کار</th>
                                    <th class="text-center">تلفن همراه</th>
                                    <th class="text-center">آدرس منزل</th>
                                    <th class="text-center">آدرس محل کار</th>

                                </tr>
                                </thead>
                                <tbody id="patient_result">

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
{{--                            <button type="button" class="btn btn-primary" id="addButton_all">افزودن </button>--}}
                            <button type="button" class="btn btn-info" id="editButton_all">ویرایش</button>
                            <button type="button" class="btn btn-danger" id="destroyButton_all">حذف (غیرفعال سازی) </button>
                            <form method="post"  id="delete-patient-form-allshow" action="{{url('dashboard/del_item/patient')}}" style="display: none">
                                @csrf
                                <input type="hidden" id="patient_item_allshow" name="id" value="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')

    <script>
        {{--let url_path = '{{url("/account")}}';--}}
        let url_path = "{{url('/dashboard/patients')}}";
        let edit_form = "{{url('/dashboard/patients')}}";
        let del_form = "{{url('/dashboard/dele_item/patients')}}";
    </script>
    <script>
        function showAllPatientTable() {
            console.log({'patient': 'all'})

            let tableId=0;
            var table = $('#allPatientTable').DataTable(
                {
                    ajax: {
                        url: url_path,
                        type: 'GET'
                    },
                    columns: [
                        { data: 'fileNumber' },
                        { data: 'firstName' },
                        { data: 'lastName' },
                        { data: 'sureName' },
                        { data: 'fatherName' },
                        { data: 'certificateId' },
                        { data: 'melliCode' },
                        { data: 'email' },
                        { data: 'gender' },
                        { data: 'marriedStatus' },
                        { data: 'sms' },
                        { data: 'file_start_date' },
                        { data: 'bloodGroup' },
                        { data: 'pregnancy' },
                        { data: 'surgery' },
                        { data: 'description' },
                        { data: 'education' },
                        { data: 'job' },
                        { data: 'birthDate' },
                        { data: 'birthLocation' },
                        { data: 'homePhone' },
                        { data: 'workPhone' },
                        { data: 'mobile' },
                        { data: 'homeAddress' },
                        { data: 'workAddress' },
                    ],
                    processing: true,
                    serverSide: true,
                    orderCellsTop: true,
                    fixedHeader: true,
                    scrollX: true,
                    select: {
                        item: 'row'
                    },
                    "tableTools": {
                        "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                    },
                    "language": {
                        "sProcessing": "درحال پردازش...",
                        "sLengthMenu": "نمایش محتویات _MENU_",
                        "sZeroRecords": "موردی یافت نشد",
                        "sInfo": "نمایش _START_ تا _END_ از مجموع _TOTAL_ مورد",
                        "sInfoEmpty": "تهی",
                        "sInfoFiltered": "(فیلتر شده از مجموع _MAX_ مورد)",
                        "sInfoPostFix": "",
                        "sSearch": "جستجو:",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "ابتدا",
                            "sPrevious": "قبلی",
                            "sNext": "بعدی",
                            "sLast": "انتها"
                        }
                    }
                }
            );
            table.on('select', function (e,dt,type,indexes) {
                if (type === 'row') {
                    var selectedRow = table.row('.selected').data();
                    tableId = selectedRow['id'];
                    console.log({'select row': selectedRow , 'selected id': tableId });

                }
            });


            document.querySelector('#editButton_all').addEventListener('click', function () {
                var selectedId= tableId;
                // alert('row id: ' + tableId + ' is selected');
                console.log({'edit url' : edit_form +'/'+ selectedId+'/edit'})
                window.location.href= edit_form +'/'+ selectedId+'/edit'
            });

            document.querySelector('#destroyButton_all').addEventListener('click', function () {
                let delSelectedId= tableId;
                // alert('row id: ' + tableId + ' is selected');
                console.log({'del url' : del_form })

                Swal({
                    text: 'از حذف مطمئن هستید؟',
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'بله',
                    showCancelButton: true,
                    cancelButtonText: 'انصراف'
                })
                    .then(function (isConfirm) {
                        if (isConfirm.value) {
                            // console.log({'swal': delSelectedId})
                            $('#patient_item_allshow').val(delSelectedId)
                            $('#delete-patient-form-allshow').submit();
                        }
                    });
            });
            // console.log({'row is': table.row(':eq(0)', { page: 'current' }).select() });
            //

            $("#showOne").hide();
            $('#showAll').show();
        }

    </script>
    <script>

    </script>
    <script>
        $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

        });

        $.ajax({

            type: "GET",

            url: '/dashboard/patients',

            dataType: 'json',

            success: function (res) {
                var patients= res['data'];

                var selectBox= '<label class="control-label" for="properties"> نام بیمار یا شماره پرونده </label><div class="form-group">' +
                    '<label class="control-label" for="brand_product_select"> سازنده </label>' +
                    '<select data-placeholder="سازنده مورد نظر را انتخاب کنید"' +
                    'class="form-control chosen-select chosen-rtl"' +
                    'tabindex="2" id="patient_selectbox"' +
                    'name="manufacture"' +
                    'title="انتخاب سازنده قطعه" id="patientSelect" onChange="showPatientRecord(this.value);">' +
                    '<option value="">انتخاب کنید</option>' ;

                    $.each(patients , function (key, value) {
                        selectBox+= '<option value="'+ value['id']+'">'+value['firstName']+' ' +value['lastName']+'</option>';

                    });

                    selectBox+= '</select>' +
                    '</div>';

                    $('#patientSelectbox').html(selectBox);

                    //chosen select box config
                    $('#patient_selectbox').chosen({
                      // allow_single_deselect: true
                      width: "100%",
                     });
            }
        })

    </script>
    <script>
        function showPatientRecord (patientId){
            var patient_url= url_path+'/'+patientId
            let tableId2=0;
            console.log({'patient': patientId, 'show one': 'ok', 'this url': patient_url});

            $('#patientTable').dataTable().fnClearTable();
            $('#patientTable').dataTable().fnDestroy();

            var tablePatient = $('#patientTable').DataTable(
                {
                    ajax: {
                        url: patient_url,
                        type: 'GET'
                    },
                    columns: [
                        { data: 'fileNumber' },
                        { data: 'firstName' },
                        { data: 'lastName' },
                        { data: 'sureName' },
                        { data: 'fatherName' },
                        { data: 'certificateId' },
                        { data: 'melliCode' },
                        { data: 'email' },
                        { data: 'gender' },
                        { data: 'marriedStatus' },
                        { data: 'sms' },
                        { data: 'file_start_date' },
                        { data: 'bloodGroup' },
                        { data: 'pregnancy' },
                        { data: 'surgery' },
                        { data: 'description' },
                        { data: 'education' },
                        { data: 'job' },
                        { data: 'birthDate' },
                        { data: 'birthLocation' },
                        { data: 'homePhone' },
                        { data: 'workPhone' },
                        { data: 'mobile' },
                        { data: 'homeAddress' },
                        { data: 'workAddress' },
                    ],
                    processing: true,
                    serverSide: true,
                    orderCellsTop: true,
                    fixedHeader: true,
                    scrollX: true,
                    retrieve: true,
                    select: {
                        item: 'row'
                    },
                    "tableTools": {
                        "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                    },
                    "language": {
                        "sProcessing": "درحال پردازش...",
                        "sLengthMenu": "نمایش محتویات _MENU_",
                        "sZeroRecords": "موردی یافت نشد",
                        "sInfo": "نمایش _START_ تا _END_ از مجموع _TOTAL_ مورد",
                        "sInfoEmpty": "تهی",
                        "sInfoFiltered": "(فیلتر شده از مجموع _MAX_ مورد)",
                        "sInfoPostFix": "",
                        "sSearch": "جستجو:",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "ابتدا",
                            "sPrevious": "قبلی",
                            "sNext": "بعدی",
                            "sLast": "انتها"
                        }
                    }
                }
            );
            tablePatient.on('select', function (e,dt,type,indexes) {
                if (type === 'row') {
                    var selectedRow = tablePatient.row('.selected').data();
                    tableId2 = selectedRow['id'];
                    console.log({'select row': selectedRow , 'selected id': tableId2 });

                }
            });

            document.querySelector('#editButton_one').addEventListener('click', function () {
                var selectedId= tableId2;
                // alert('row id: ' + tableId + ' is selected');
                console.log({'edit url' : edit_form +'/'+ selectedId+'/edit'})
                window.location.href= edit_form +'/'+ selectedId+'/edit'
            });

            document.querySelector('#destroyButton_one').addEventListener('click', function () {
                let delSelectedId2= tableId;
                // alert('row id: ' + tableId + ' is selected');
                console.log({'del url' : del_form })

                Swal({
                    text: 'از حذف مطمئن هستید؟',
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'بله',
                    showCancelButton: true,
                    cancelButtonText: 'انصراف'
                })
                    .then(function (isConfirm) {
                        if (isConfirm.value) {
                            // console.log({'swal': delSelectedId})
                            $('#patient_item_oneShow').val(delSelectedId2)
                            $('#delete-patient-form-oneShow').submit();
                        }
                    });
            });

            $("#showOne").show();
            $('#showAll').hide();
        }

    </script>
@endsection
