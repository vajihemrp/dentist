@extends('layout.master')
@section('page')
    ثبت دندانپزشک
@endsection
@section('content')

    <div class="col-lg-12" dir="rtl" >
        <section class="panel">
            <div class="row">
                <div class="col-xs-12">
                    <header class="panel-heading">نمایش اطلاعات</header>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dentistTable" class="table table-hover table-bordered dataTables-example align-items-center"
                                   cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" >شماره پرونده</th>
                                    <th class="text-center" >نام</th>
                                    <th class="text-center">نام خانوادگی</th>
                                    <th class="text-center">نظام پزشکی</th>
                                    <th class="text-center">درصد</th>
                                    <th class="text-center">تاریخ تولد</th>
                                    <th class="text-center">جنسیت</th>
                                    <th class="text-center">نام بانک</th>
                                    <th class="text-center">شماره حساب</th>
                                    <th class="text-center">شماره کارت</th>
                                    <th class="text-center">تخصص</th>
                                    <th class="text-center">تبحر</th>
                                    <th class="text-center">دانشگاه</th>
                                    <th class="text-center">معرف</th>
                                    <th class="text-center">سابقه</th>
                                    <th class="text-center">شماره بیمه</th>
                                    <th class="text-center">تلفن</th>
                                    <th class="text-center">موبایل</th>
                                    <th class="text-center">پیامک خلاصه عملکرد</th>
                                    <th class="text-center">پیامک قرار ملاقات</th>
                                    <th class="text-center">آدرس منزل</th>
                                    <th class="text-center">آدرس محل کار</th>

                                </tr>
                                </thead>
                                <tbody id="dentist_result">

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-primary" id="addButton_one" onclick="$('#dentistForm').submit();">افزودن </button>
                            <button type="button" class="btn btn-info" id="editButton_one">ویرایش</button>
                            <button type="button" class="btn btn-danger" id="destroyButton_one">حذف (غیرفعال سازی) </button>
                            <form method="post"  id="delete-patient-form-oneShow" action="{{url('dashboard/del_item/patient')}}" style="display: none">
                                @csrf
                                <input type="hidden" id="patient_item_oneShow" name="id" value="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-12" dir="rtl">
        <section class="panel">
            <div class="row">

                <div class="col-xs-12" dir="rtl">
                    <header class="panel-heading">
                        ثبت دندانپزشک
                    </header>
                    <div class="panel-body">
                        @if(count($errors))
                            <div id="backward_errors">
                                <div class="alert alert-danger col-lg-12 text-right" dir="rtl">
                                    <strong>خطا!</strong> لطفا موارد ذیل را چک کنید
                                    <br/>
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        @endif
                        <div class="form">
                            <form class="cmxform form-horizontal tasi-form" id="dentistForm"  role="form" action="{{url('dashboard/dentists')}}" method="post" enctype="multipart/form-data" >
                                @csrf
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">نام</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="firstname" name="firstName" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="lastname" class="control-label col-lg-2">نام خانوادگی</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="lastname" name="lastName" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surename" class="control-label col-lg-2">نظام پزشکی</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="medical_code" name="medical_code" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="fatherName" class="control-label col-lg-2">درصد</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="percent" name="percent" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="fromDate1" class="control-label col-lg-2">تاریخ تولد</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control" id="birthDate"
                                                   data-mddatetimepicker="true" data-trigger="click" onkeypress="return false;"
                                                   data-targetselector="#birthDate" data-groupid="group2" data-fromdate="true"
                                                   data-enabletimepicker="false" data-placement="top" name="birth_date"
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">جنسیت</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="gender" id="optionsRadios1" value="1" checked>
                                                    مرد
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="gender" id="optionsRadios2" value="0">
                                                    زن
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="certificateId" class="control-label col-lg-2">نام بانک</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="bank_name" name="bank_name" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="melliCode" class="control-label col-lg-2">شماره حساب</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="bank_account_number" name="bank_account_number" type="text" />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="email" class="control-label col-lg-2">شماره کارت</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="bank_cart_number" name="bank_cart_number" type="text" />
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">تخصص</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="specialist" name="specialist" type="text" required />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="lastname" class="control-label col-lg-2">تبحر</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="skill" name="skill" type="text" required />
                                        </div>
                                    </div>

                                    <div class="form-group ">
                                        <label for="surename" class="control-label col-lg-2">دانشگاه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="university" name="university" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surename" class="control-label col-lg-2">معرف</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="reagent" name="reagent" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="surename" class="control-label col-lg-2">سابقه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="experience" name="experience" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="fatherName" class="control-label col-lg-2">شماره بیمه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="insurance_code" name="insurance_code" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="workPhone" class="control-label col-lg-2">تلفن</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="phone" name="phone" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="mobile" class="control-label col-lg-2">تلفن همراه</label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="mobile" name="mobile" type="text" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">پیامک خلاصه عملکرد</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms_operation" id="optionsRadios1" value="1" checked>
                                                    بله
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms_operation" id="optionsRadios2" value="0">
                                                    خیر
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">پیامک قرار ملاقات</label>
                                        <div class="col-lg-10">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms_visit" id="optionsRadios3" value="1" checked>
                                                    بله
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="sms_visit" id="optionsRadios4" value="0">
                                                    خیر
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <div class="form-group ">
                                        <label for="homeAddress" class="control-label col-lg-2">آدرس منزل</label>
                                        <div class="col-lg-10">
                                            <textarea name="homeAddress" class="form-control description_box" rows="3"
                                                      id="homeAddress"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="workAddress" class="control-label col-lg-2">آدرس محل کار</label>
                                        <div class="col-lg-10">
                                            <textarea name="workAddress" class="form-control description_box" rows="3"
                                                      id="workAddress"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">ترتیب نمایش</label>
                                        <div class="col-lg-10">
                                            <input class=" form-control" id="showing_sort" name="showing_sort" type="text" required maxlength="2"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script>
        let url_path = "{{url('/dashboard/dentists')}}";
        let tableId=0;
        var table = $('#dentistTable').DataTable(
            {
                ajax: {
                    url: url_path,
                    type: 'GET'
                },
                columns: [
                    { data: 'firstName' },
                    { data: 'lastName' },
                    { data: 'medical_code'},
                    { data: 'percent' },
                    { data: 'birthDate' },
                    { data: 'gender' },
                    { data: 'bank_name' },
                    { data: 'bank_account_number' },
                    { data: 'bank_cart_number' },
                    { data: 'specialist' },
                    { data: 'skill' },
                    { data: 'university' },
                    { data: 'reagent' },
                    { data: 'experience'},
                    { data: 'insurance_code' },
                    { data: 'phone' },
                    { data: 'mobile' },
                    { data: 'sms_operation' },
                    { data: 'sms_visit' },
                    { data: 'homeAddress' },
                    { data: 'workAddress' },
                    { data: 'showing_sort' },
                ],

                processing: true,
                serverSide: true,
                orderCellsTop: true,
                fixedHeader: true,
                scrollX: true,
                select: {
                    item: 'row'
                },
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "sProcessing": "درحال پردازش...",
                    "sLengthMenu": "نمایش محتویات _MENU_",
                    "sZeroRecords": "موردی یافت نشد",
                    "sInfo": "نمایش _START_ تا _END_ از مجموع _TOTAL_ مورد",
                    "sInfoEmpty": "تهی",
                    "sInfoFiltered": "(فیلتر شده از مجموع _MAX_ مورد)",
                    "sInfoPostFix": "",
                    "sSearch": "جستجو:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "ابتدا",
                        "sPrevious": "قبلی",
                        "sNext": "بعدی",
                        "sLast": "انتها"
                    }
                }
            }
        );
        table.on('select', function (e,dt,type,indexes) {
            if (type === 'row') {
                var selectedRow = table.row('.selected').data();
                tableId = selectedRow['id'];
                console.log({'select row': selectedRow , 'selected id': tableId });

            }
        });
    </script>
@endsection
